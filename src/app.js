require('dotenv-flow').config({
    node_env: process.env.NODE_ENV,
    default_node_env: 'development'
})

const express = require('express')
const helmet = require('helmet')
const debug = require('debug')('app:main')
const morgan = require('morgan')
const winston = require('./config/components/winston')
const cookieParser = require('cookie-parser')
const config = require('./config/index')

const app = express()

app.use(express.urlencoded({ extended: false }))
app.use(express.json())

app.use(function (error, req, res, next) {
    debug('client request contained invalid JSON syntax')
    if (error instanceof SyntaxError) return res.status(400).send({ error: 'Request syntax invalid'});
    next();
});
app.use(helmet())
app.use(cookieParser())

if (process.env.NODE_ENV !== 'test') app.use(morgan('combined', { stream: winston.stream }))

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

// Enable pre-flight request check for X-Requested-With == XMLHttpRequest
app.options((req, res, next) => {
    if (req.xhr) {
        next()
    } else {
        debug('client request was not Ajax')
        res.status(400).send('bad request')
    }
})


app.use('/api/token', require('./routes'))


const port = config.server.port
app.listen(port, () => debug(`${config.server.appName} listening on port ${port}!`))

module.exports = app
