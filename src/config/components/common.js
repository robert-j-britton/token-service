'use strict'

const joi = require('@hapi/joi')

const configSchema = joi.object({
    NODE_ENV: joi.string()
        .allow('development', 'production', 'test')
        .default('development'),
    PORT: joi.number()
}).unknown()
  .required()

const { error, value: envVars } = configSchema.validate(process.env)
if (error) {
    const errorMessage = `Config validation error: ${error.message}`
    console.log(errorMessage)
    throw new Error(errorMessage)
}

const config = {
    server: {
        appName: 'Token Service',
        isDevelopment: envVars.NODE_ENV === 'development',
        isProduction: envVars.NODE_ENV === 'production',
        isTest: envVars.NODE_ENV === 'test',
        port: envVars.PORT || 35897
    }
}

module.exports = config
