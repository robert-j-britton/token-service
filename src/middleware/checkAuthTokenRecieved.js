const Global = require('../global')

const checkExists = (req, res, next) => {
    let token = req.headers.authorization

    if (!token || token === '') {
        return res.status(400).send({
            error: {
                message: Global.MISSING_TOKEN
            }
        })
    }

    next()
}

const format = (req, res, next) => {
    let token = req.headers.authorization

    token = token.split(' ')

    if(token.length !== 2) {
        return res.status(400).send({
            error: {
                message: Global.MISSING_PREFIX_OR_TOKEN
            }
        })
    }

    req.token = token[1]
    next()
}

module.exports = [checkExists, format, (req, res, next) => next()];
