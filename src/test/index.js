'use strict'

const appRoot = require('app-root-path')
const assert = require('chai').assert

const envPath = `${appRoot}/.env.local`

require('dotenv').config({
    path: envPath
})

const TokenService = require('../services/TokenService')


describe('generateJwt(payload, expiresIn, algorithm)', function() {
    it('should return a valid jwt when no args are passed in', function(done) {
        TokenService.generateJwt()
            .then(token => {
                assert.isString(token, 'token is not a string')
                done()
            })

    })
    it('should return a valid jwt when payload is a string', function(done) {
        TokenService.generateJwt("Hello")
            .then(token => {
                assert.isString(token, 'token is not a string')
                done()
            })
    })
    it('should return valid jwt when payload is an array', function(done) {
        TokenService.generateJwt([1, 2, 3, 4])
            .then(token => {
                assert.isString(token, 'token is not a string')
                done()
            })
    })
    it('should return valid jwt when payload is a number', function(done) {
        TokenService.generateJwt(12345)
            .then(token => {
                assert.isString(token, 'token is not a string')
                done()
            })
    })
})

describe('generateJwtSplit(payload, expiresIn, algorithm)', function() {
    it('should return a valid split jwt when no args are passed in', function(done) {
        TokenService.generateJwtSplit()
            .then(token => {
                assert.isObject(token, 'token is not an object')
                assert.hasAnyKeys(token, 'header_payload', 'missing header_payload')
                done()
            })
    })
    it('should return a valid split jwt when payload is a string', function(done) {
        TokenService.generateJwtSplit("Hello")
            .then(token => {
                assert.isObject(token, 'token is not an object')
                assert.hasAnyKeys(token, 'header_payload', 'missing header_payload')
                done()
            })
    })
    it('should return valid split jwt when payload is an array', function(done) {
        TokenService.generateJwtSplit([1, 2, 3, 4])
            .then(token => {
                assert.isObject(token, 'token is not an object')
                assert.hasAnyKeys(token, 'header_payload', 'missing header_payload')
                done()
            })
    })
    it('should return valid split jwt when payload is a number', function(done) {
        TokenService.generateJwtSplit(12345)
            .then(token => {
                assert.isObject(token, 'token is not an object')
                assert.hasAnyKeys(token, 'header_payload', 'missing header_payload')
                done()
            })
    })
})

describe('validateJwt(token, algorithms)', function() {
    let validJwt;

    before(function(done) {
        TokenService.generateJwt({ name: 'Nedd Stark' })
            .then(token => {
                validJwt = token
                done()
            })
    })
    it('should return a decoded jwt object when valid jwt passed in', function(done) {
        TokenService.validateJwt(validJwt)
            .then(token => {
                assert.isObject(token, 'token is not an object')
                assert.hasAllKeys(token, ['name', 'exp', 'iat'])
                done()
            })
    })
    it('should return undefined when invalid jwt passed in', function(done) {
        TokenService.validateJwt("notvalid")
            .catch(error => {
                assert.isObject(error, 'error is not an object')
                done()
            })
    })
})
