const router = require('express').Router()
const TokenService = require('../services/TokenService')
const checkAuthTokenRecieved = require('../middleware/checkAuthTokenRecieved')


// return a signed JWT containing the given payload if specified.
router.post('/generate', (req, res) => {
    const { payload, expires } = req.body
    TokenService.generateJwt(payload || {}, expires )
        .then(token => {
            res.send({ token })
        })
        .catch(error => {
            res.status(500).send({ error: error.name });
        })
})

// return a 2-way split JWT. Useful for storing across 2 cookies.
router.post('/generate/split', (req, res) => {
    const { payload, expires } = req.body

    TokenService.generateJwtSplit( payload || {}, expires )
        .then(token => {
            res.send({ token })
        })
        .catch(error => {
            res.status(500).send({ error: error.name });
        })
})

// Verifies a JWT recieved from within the authorization header.
router.post('/verify', checkAuthTokenRecieved, (req, res) => {
    const { token } = req

    TokenService.validateJwt(token)
        .then(decoded => {
            res.send({ decoded })
        })
        .catch(error => {
            if (error.name === 'TokenExpiredError') return res.status(401).send({ error: error.message })
            if(error.name === 'JsonWebTokenError') return res.status(400).send({ error: error.message })
            res.status(500).send({ error: error.name })
        })
})

module.exports = router
