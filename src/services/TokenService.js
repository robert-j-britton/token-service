const jwt = require('jsonwebtoken')
const appRoot = require('app-root-path')
const fs = require('fs')
const _ = require('lodash')
const config = require('../config')


module.exports.generateJwt = generateJwt
module.exports.generateJwtSplit = generateJwtSplit
module.exports.validateJwt = validateJwt

function generateJwt(payload, expiresIn = "12h") {
    return new Promise((resolve, reject) => {
        // Store payload in a value property inside an object if payload isn't already an object
        if (!_.isPlainObject(payload)) payload = { value: payload }

        try {
            const privateKey = fs.readFileSync(`${appRoot}/keys/private.key`, 'utf8')
            const token = jwt.sign(payload, privateKey, {
                expiresIn,
                algorithm: "RS256"})
            resolve(token)
        } catch(error) {
            // TODO: Log error
            if (!config.server.isTest) console.log('token-service-error: ', error)
            reject(error)
        }
    })
}

function generateJwtSplit(payload, expiresIn = "12h") {
    return new Promise(async (resolve, reject) => {
        try {
            let token = await generateJwt(payload, expiresIn)

            if (!token) return undefined

            token = splitJwt(token)
            resolve({
                header_payload: token[0],
                signature: token[1]
            })
        } catch(error) {
            if (!config.server.isTest) console.log('token-service error: ', error)
            reject(error)
        }
    })
}

function validateJwt(token) {
    return new Promise((resolve, reject) => {
        try {
            const publicKey = fs.readFileSync(`${appRoot}/keys/public.key`, 'utf8')
            const decoded = jwt.verify(token, publicKey, {  algorithms: ["RS256"] })
            resolve(decoded)
        } catch (error) {
            if (!config.server.isTest) console.log('token-service error: ', error.message)
            reject(error)
        }
    })
}

function splitJwt(token) {
    let token_split = token.split('.')
    const signature = token_split.pop()
    token_split = token_split.join('.').split(' ')
    token_split.push(`.${signature}`)
    return token_split
}
