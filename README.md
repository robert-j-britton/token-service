[![CircleCI](https://circleci.com/bb/robert-j-britton/token-service.svg?style=svg)](https://circleci.com/bb/robert-j-britton/token-service) [![Coverage Status](https://coveralls.io/repos/bitbucket/robert-j-britton/token-service/badge.svg?branch=master)](https://coveralls.io/bitbucket/robert-j-britton/token-service?branch=master)

# Token Service

A micro service that generates JWT's. Tokens can be requested as a whole or as a 2-way split which can be useful for distributing to a client across 2 cookies. The service also provides JWT verification to ensure JWT's received were created by this service.
Ideally you should swap out both public and private keys in the run method for security purposes.

### Build and Run Development Project
---
Create a `keys` directory in project root. This directory must contain both `private.key` and `public.key` files which you must create. Generate then add RSA 256 private and public keys to the respective files before running.

If you want to view logs within a persisted file, be sure there is a `logs` directory in the root of the project.

```
docker-compose up -d
```
