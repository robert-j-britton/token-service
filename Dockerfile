FROM node:10-alpine

WORKDIR /app

COPY ./src/package*.json ./

RUN npm install

COPY ./src .

CMD [ "node", "app.js" ]
